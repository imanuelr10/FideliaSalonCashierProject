<?php
$conn = oci_connect('nuel', 'faceless','localhost/XE');
                if(isset($_POST['login'])){

                    $username = $_POST['username'];
                    $password = $_POST['password'];

                    $query = oci_parse($conn, "SELECT * FROM ADMIN WHERE USERNAME = '$username' AND pass = '$password'");
                    oci_execute($query);
                    //$rs = $conn->query($query);
                    //$num = $rs->num_rows;
                    $rows = oci_fetch_array($query,OCI_ASSOC);
                    if($rows){
                        session_start();
                        $_SESSION['username'] = $username;
                        $_SESSION['password'] = $password;
                        //$_SESSION['nama'] = $_GET['nama'];
                        echo "<script type = \"text/javascript\">
                                    alert(\"Login Successful, Welcome $username\");
                                    window.location = (\"cashierapps.php\")
                                    </script>";
                    } else{
                        echo "<script type = \"text/javascript\">
                                    alert(\"Login Failed\");
                                    window.location = (\"adminlogin.php\")
                                    </script>";
                    }
                }
            ?>



<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cashier Application</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="POST" action="adminlogin.php" enctype="multipart/form-data">
					<span class="login100-form-title p-b-70">
						Admin Cashier
					</span>
					<span class="login100-form-avatar">
						<img src="fideliasalon.png" alt="AVATAR">
					</span>

					<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate = "Enter username">
						<input class="input100" type="text" name="username">
						<span class="focus-input100" data-placeholder="Username"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-50" data-validate="Enter password">
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" name="login">
							Login
						</button>
					</div>
					<br>
					<div class="container-login100-form-btn">
						<a href="admin/admininsert.php" class="login100-form-btn" name="register" role="button">Register</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>