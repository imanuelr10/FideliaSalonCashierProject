<?php
session_start();
$conn = oci_connect('nuel', 'faceless','localhost/XE');
$query = oci_parse($conn, "SELECT * FROM PELAYANAN");
oci_execute($query);
//$
?>

<!DOCTYPE html>
<html>
<head>
	    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Cashier Application</title>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <script type="text/javascript" src="print.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="print.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.js">
    <link rel="stylesheet" type="text/css" href="https://printjs-4de6.kxcdn.com/print.min.css">
</head>
<body>
	<div class="container">
	<div class="row">
        <div class="col-sm-12">
            <center>
            <legend style="background-color: #9D0C3F; color: white">Fidelia Salon Cashier</legend>
            </center>
        </div>
        <!-- panel preview -->
        <div class="col-sm-5">
            <h4>Add payment:</h4>
            <div class="panel panel-default">
                <form action="transaksiinsert.php" enctype="multipart/form-data" id="printJs-form">
                <div class="panel-body form-horizontal payment-form">
                    <div class="form-group">
                        <label for="concept" class="col-sm-3 control-label">Admin</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="admin_cashier" name="admin_cashier" value="<?php echo $_SESSION['username']; ?>" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date" class="col-sm-3 control-label">Date</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="date" name="date">
                        </div>
                    </div>   
                    <div class="form-group">
                        <label for="concept" class="col-sm-3 control-label">Customer</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nama_customer" name="nama_customer">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Treatment</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="treatment" name="treatment">
                                <?php
                                    while(($res = oci_fetch_array($query, OCI_ASSOC))){
                                ?>
                                    <option value="<?php echo htmlentities($res['NAMA_PRODUK']); ?>"><?php echo htmlentities($res['NAMA_PRODUK']); ?></option>
                                <?php
                                    }
                                ?>
                                <option>Unpaid</option>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="amount" class="col-sm-3 control-label">Amount</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <a href="logout.php" class="btn btn-danger" role="button">
                                <span class="glyphicon glyphicon-remove"></span>Logout
                            </a>
                            <button type="button" class="btn btn-info preview-add-button" onclick="addItem()">
                                <span class="glyphicon glyphicon-plus"></span>Tambah
                            </button>
                        </div>
                        <div class="col-sm-12 text-left">
                            
                        </div>
                    </div>
                </div>
            </form>
            </div>            
        </div> <!-- / panel preview -->
        <div class="col-sm-7">
            <h4>Preview:</h4>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table preview-table">
                            <thead>
                                <tr>
                                    <th>Treatment</th>
                                    <th>Amount</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody id="addItemPlace">
                            </tbody> <!-- preview content goes here-->
                        </table>
                    </div>                            
                </div>
            </div>
            <div class="row text-right">
                <div class="col-xs-12">
                    <h4>Total: <strong><span class="preview-total" value='preview-total'></span></strong></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <hr style="border:1px dashed #dddddd;">
                    <!--<button type="button" class="btn btn-primary btn-block" name="submit" type="submit">Submit all and finish</button>-->
                    <a href="receipt.php" class="btn btn-primary btn-block" role="button" type="submit">Submit all and Finish</a>
                </div>                
            </div>
        </div>
	</div>
</div>
<script type="text/javascript">
	function calc_total(){
    var sum = 0;
    $('.input-amount').each(function(){
        sum += parseFloat($(this).text());
    });
    $(".preview-total").text(sum);    
}

    function addItem(){
        var nama_customer = $("#nama_customer").val(),
            date = $("#date").val(),
            treatment = $("#treatment").val(),
            amount = $("#amount").val();

            //alert(date + nama_customer + treatment + amount);
        $.ajax({
        url: 'transaksiinsert.php',
        type: 'POST',
        dataType: 'html',
        data: "tanggal="+ date + "&nama="+nama_customer +"&pelayanan="+ treatment + "&jumlah="+amount,
        success: function(data) {
                   $("#addItemPlace").append(data);
                   //alert(data);
                }
        });
    }
$(document).on('click', '.input-remove-row', function(){ 
    var tr = $(this).closest('tr');
    tr.fadeOut(200, function(){
    	tr.remove();
	   	calc_total()
	});
});

/*$(function(){
    $('.preview-add-button').click(function(){
        var form_data = {};
        //form_data["concept"] = $('.payment-form input[name="concept"]').val();
        form_data["treatment"] = $('.payment-form input[name="treatment"]').val();
        form_data["amount"] = parseFloat($('.payment-form input[name="amount"]').val());
        //form_data["status"] = $('.payment-form #status option:selected').text();
        //form_data["date"] = $('.payment-form input[name="date"]').val();
        form_data["remove-row"] = '<span class="glyphicon glyphicon-remove"></span>';
        var row = $('<tr></tr>');
        $.each(form_data, function( type, value ) {
            $('<td class="input-'+type+'"></td>').html(value).appendTo(row);
        });
        $('.preview-table > tbody:last').append(row); 
        calc_total();
    });  
});*/
</script>
</body>
</html>